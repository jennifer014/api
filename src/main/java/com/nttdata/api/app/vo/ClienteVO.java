package com.nttdata.api.app.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class ClienteVO{

    private Long id;

    @NotNull(message = "El campo nombre no puede ser nulo")
    @NotBlank(message = "El campo nombre no puede estar vacio")
    private String nombre;

    @NotNull(message = "El campo genero no puede ser nulo")
    @NotBlank(message = "El campo genero no puede estar vacio")
    private String genero;

    private Integer edad;

    @NotNull(message = "El campo identificacion no puede ser nulo")
    @NotBlank(message = "El campo identificacion no puede estar vacio")
    private String identificacion;

    @NotNull(message = "El campo direccion no puede ser nulo")
    @NotBlank(message = "El campo direccion no puede estar vacio")
    private String direccion;

    @NotNull(message = "El campo telefono no puede ser nulo")
    @NotBlank(message = "El campo telefono no puede estar vacio")
    private String telefono;

    @NotNull(message = "El campo password no puede ser nulo")
    @NotBlank(message = "El campo password no puede estar vacio")
    private String password;

    @NotNull(message = "El campo estado no puede ser nulo")
    private Boolean estado;
}

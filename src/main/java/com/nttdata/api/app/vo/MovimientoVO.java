package com.nttdata.api.app.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
public class MovimientoVO {

    private Long id;

    private Date fecha;

    private String tipoMovimiento;

    @NotNull(message = "El campo valor no puede ser nulo")
    private Float valor;

    private Float saldo;

    @NotNull(message = "El campo estado no puede ser nulo")
    private Boolean estado;

    private Long cuentaId;

    @NotNull(message = "El campo cuenta no puede ser nulo")
    private CuentaVO cuenta;

    @NotNull(message = "El campo fecha de transaccion no puede ser nulo")
    @NotBlank(message = "El campo fecha de transaccion no puede ser nulo")
    private String fechaTransaccion;


}

package com.nttdata.api.app.repository;

import com.nttdata.api.app.dto.ClienteDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteDTO, Long> {
}

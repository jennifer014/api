package com.nttdata.api.app.repository;

import com.nttdata.api.app.dto.MovimientoDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MovimientoRepository extends JpaRepository<MovimientoDTO, Long> {

    @Query("select u from MovimientoDTO u JOIN u.cuenta c where c.clienteId = :clienteId and  u.fecha between :initDate and :endDate order by u.fecha desc ")
    List<MovimientoDTO> reportByDateAndClient(Long clienteId, Date initDate, Date endDate);

    @Query("select u from MovimientoDTO u where u.cuentaId = :cuentaId order by u.id desc")
    List<MovimientoDTO> findMovimientosByCuentaId(Long cuentaId);


}

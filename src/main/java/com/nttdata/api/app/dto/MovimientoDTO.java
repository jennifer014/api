package com.nttdata.api.app.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="t_movimientos")
@Getter
@Setter
public class MovimientoDTO {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "FECHA", nullable = false)
    private Date fecha;

    @Column(name = "TIPOMOVIMIENTO", nullable = false)
    private String tipoMovimiento;

    @Column(name = "VALOR", nullable = false)
    private Float valor;

    @Column(name = "SALDO")
    private Float saldo;

    @Column(name = "ESTADO", nullable = false)
    private Boolean estado;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(targetEntity = CuentaDTO.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUENTAID",  referencedColumnName="ID", insertable=false, updatable=false, nullable = false)
    private CuentaDTO cuenta;


    @Column(name="CUENTAID", nullable = false)
    private Long cuentaId;

}

package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.PersonaDTO;
import com.nttdata.api.app.vo.ClienteVO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PersonaMapper {

    PersonaMapper MAPPER = Mappers.getMapper( PersonaMapper.class );

    @InheritInverseConfiguration
    PersonaDTO toDto(ClienteVO clienteVO);


}

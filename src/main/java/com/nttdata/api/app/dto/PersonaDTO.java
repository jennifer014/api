package com.nttdata.api.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="t_persona")
@Getter
@Setter
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonaDTO {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE", nullable = false)
    private String nombre;

    @Column(name = "GENERO", nullable = false)
    private String genero;

    @Column(name = "EDAD", nullable = true)
    private Integer edad;

    @Column(name = "IDENTIFICACION", nullable = false)
    private String identificacion;

    @Column(name = "DIRECCION", nullable = false)
    private String direccion;

    @Column(name = "TELEFONO", nullable = false)
    private String telefono;
}

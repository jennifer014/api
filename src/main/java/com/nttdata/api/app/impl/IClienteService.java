package com.nttdata.api.app.impl;

import com.nttdata.api.app.dto.ClienteDTO;

public interface IClienteService {

    ClienteDTO obtenerCliente(Long clientId);

    ClienteDTO guardarCliente(ClienteDTO cliente);

    void eliminarCliente(Long clientId);

}

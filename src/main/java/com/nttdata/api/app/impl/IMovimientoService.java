package com.nttdata.api.app.impl;

import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.dto.MovimientoDTO;

import java.util.Date;
import java.util.List;

public interface IMovimientoService {

    MovimientoDTO obtenerMovimiento(Long movimientoId);

    MovimientoDTO guardarMovimiento(MovimientoDTO movimiento);

    void eliminarMovimiento(Long movimientoId);

    List<MovimientoDTO> buscarUltimosMovimientos(Long cuentaId);

    List<MovimientoDTO> reportePorClienteFecha(Long clienteId, Date dateInit, Date dateEnd);

}

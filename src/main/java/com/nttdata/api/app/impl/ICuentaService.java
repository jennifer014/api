package com.nttdata.api.app.impl;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.CuentaDTO;

public interface ICuentaService {

    CuentaDTO obtenerCuenta(Long cuentaId);

    CuentaDTO guardarCuenta(CuentaDTO cuenta);

    void eliminarCuenta(Long cuentaId);

    CuentaDTO buscarCuentaPorNumeroTipo(String numeroCuenta, String tipoCuenta);

}

package com.nttdata.api.app.service;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.mapper.ClienteMapper;
import com.nttdata.api.app.impl.IClienteService;
import com.nttdata.api.app.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService implements IClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteDTO obtenerCliente(Long clientId){
        return clienteRepository.findById(clientId).orElse(null);
    }

    @Override
    public ClienteDTO guardarCliente(ClienteDTO cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public void eliminarCliente(Long clientId) {
        clienteRepository.deleteById(clientId);
    }
}

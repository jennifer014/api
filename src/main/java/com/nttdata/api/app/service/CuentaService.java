package com.nttdata.api.app.service;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.impl.IClienteService;
import com.nttdata.api.app.impl.ICuentaService;
import com.nttdata.api.app.repository.ClienteRepository;
import com.nttdata.api.app.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CuentaService implements ICuentaService {

    @Autowired
    private CuentaRepository cuentaRepository;

    public CuentaDTO obtenerCuenta(Long cuentaId){
        return cuentaRepository.findById(cuentaId).orElse(null);
    }

    @Override
    public CuentaDTO guardarCuenta(CuentaDTO cuenta) {
        return cuentaRepository.save(cuenta);
    }

    @Override
    public void eliminarCuenta(Long cuentaId) {
        cuentaRepository.deleteById(cuentaId);
    }

    @Override
    public CuentaDTO buscarCuentaPorNumeroTipo(String numeroCuenta, String tipoCuenta){
        return cuentaRepository.findByNumberAndType(
                numeroCuenta,
                tipoCuenta);
    }
}

package com.nttdata.api.app.service;

import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.dto.MovimientoDTO;
import com.nttdata.api.app.impl.ICuentaService;
import com.nttdata.api.app.impl.IMovimientoService;
import com.nttdata.api.app.repository.CuentaRepository;
import com.nttdata.api.app.repository.MovimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MovimientoService implements IMovimientoService {

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Override
    public MovimientoDTO obtenerMovimiento(Long movimientoId){
        return movimientoRepository.findById(movimientoId).orElse(null);
    }

    @Override
    public MovimientoDTO guardarMovimiento(MovimientoDTO movimiento) {
        return movimientoRepository.save(movimiento);
    }

    @Override
    public void eliminarMovimiento(Long cuentaId) {
        movimientoRepository.deleteById(cuentaId);
    }

    @Override
    public List<MovimientoDTO> buscarUltimosMovimientos(Long cuentaId) {
        return movimientoRepository.findMovimientosByCuentaId(cuentaId);
    }

    @Override
    public List<MovimientoDTO> reportePorClienteFecha(Long clienteId, Date dateInit, Date dateEnd) {
        return movimientoRepository.reportByDateAndClient(clienteId, dateInit, dateEnd);
    }


}

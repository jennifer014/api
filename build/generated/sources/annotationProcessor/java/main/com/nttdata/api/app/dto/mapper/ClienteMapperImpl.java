package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.vo.ClienteVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-25T22:43:37-0500",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 18.0.2.1 (Homebrew)"
)
public class ClienteMapperImpl implements ClienteMapper {

    @Override
    public ClienteDTO toDto(ClienteVO clienteVO) {
        if ( clienteVO == null ) {
            return null;
        }

        ClienteDTO clienteDTO = new ClienteDTO();

        clienteDTO.setId( clienteVO.getId() );
        clienteDTO.setNombre( clienteVO.getNombre() );
        clienteDTO.setGenero( clienteVO.getGenero() );
        clienteDTO.setEdad( clienteVO.getEdad() );
        clienteDTO.setIdentificacion( clienteVO.getIdentificacion() );
        clienteDTO.setDireccion( clienteVO.getDireccion() );
        clienteDTO.setTelefono( clienteVO.getTelefono() );
        clienteDTO.setPassword( clienteVO.getPassword() );
        clienteDTO.setEstado( clienteVO.getEstado() );

        return clienteDTO;
    }

    @Override
    public ClienteVO toVo(ClienteDTO clienteDTO) {
        if ( clienteDTO == null ) {
            return null;
        }

        ClienteVO clienteVO = new ClienteVO();

        clienteVO.setId( clienteDTO.getId() );
        clienteVO.setNombre( clienteDTO.getNombre() );
        clienteVO.setGenero( clienteDTO.getGenero() );
        clienteVO.setEdad( clienteDTO.getEdad() );
        clienteVO.setIdentificacion( clienteDTO.getIdentificacion() );
        clienteVO.setDireccion( clienteDTO.getDireccion() );
        clienteVO.setTelefono( clienteDTO.getTelefono() );
        clienteVO.setPassword( clienteDTO.getPassword() );
        clienteVO.setEstado( clienteDTO.getEstado() );

        return clienteVO;
    }
}

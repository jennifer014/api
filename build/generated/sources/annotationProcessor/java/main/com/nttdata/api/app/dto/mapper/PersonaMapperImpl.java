package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.PersonaDTO;
import com.nttdata.api.app.vo.ClienteVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-25T22:43:38-0500",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 18.0.2.1 (Homebrew)"
)
public class PersonaMapperImpl implements PersonaMapper {

    @Override
    public PersonaDTO toDto(ClienteVO clienteVO) {
        if ( clienteVO == null ) {
            return null;
        }

        PersonaDTO personaDTO = new PersonaDTO();

        personaDTO.setId( clienteVO.getId() );
        personaDTO.setNombre( clienteVO.getNombre() );
        personaDTO.setGenero( clienteVO.getGenero() );
        personaDTO.setEdad( clienteVO.getEdad() );
        personaDTO.setIdentificacion( clienteVO.getIdentificacion() );
        personaDTO.setDireccion( clienteVO.getDireccion() );
        personaDTO.setTelefono( clienteVO.getTelefono() );

        return personaDTO;
    }
}

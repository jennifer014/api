FROM openjdk:8-jdk-alpine

MAINTAINER Jennifer Honores "jenniferhonores014@gmail.com"

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

#EXPOSE 8080

#WORKDIR /app


#COPY build/libs/interface-root-0.0.1-SNAPSHOT.jar /api/aplication.jar

#ENTRYPOINT exec java -Xms64m -Xmx256m -jar aplication.jar

FROM openjdk:8-alpine
ADD build/libs/interface-root-0.0.1-SNAPSHOT.jar /api/app.jar
ENTRYPOINT ["/usr/bin/java", "-jar", "/api/app.jar"]
--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Debian 14.4-1.pgdg110+1)
-- Dumped by pg_dump version 14.1

-- Started on 2022-07-10 16:05:33 -05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 209 (class 1259 OID 24657)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 24658)
-- Name: t_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_cliente (
    estado boolean NOT NULL,
    password character varying(255) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.t_cliente OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 24663)
-- Name: t_cuenta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_cuenta (
    id bigint NOT NULL,
    clienteid bigint NOT NULL,
    estado boolean NOT NULL,
    numerocuenta character varying(255) NOT NULL,
    saldoinicial real NOT NULL,
    tipocuenta character varying(255) NOT NULL
);


ALTER TABLE public.t_cuenta OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 24670)
-- Name: t_movimientos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_movimientos (
    id bigint NOT NULL,
    cuentaid bigint NOT NULL,
    estado boolean NOT NULL,
    fecha timestamp without time zone NOT NULL,
    saldo real,
    tipomovimiento character varying(255) NOT NULL,
    valor real NOT NULL
);


ALTER TABLE public.t_movimientos OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 24675)
-- Name: t_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_persona (
    id bigint NOT NULL,
    direccion character varying(255) NOT NULL,
    edad integer,
    genero character varying(255) NOT NULL,
    identificacion character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    telefono character varying(255) NOT NULL
);


ALTER TABLE public.t_persona OWNER TO postgres;

--
-- TOC entry 3330 (class 0 OID 24658)
-- Dependencies: 210
-- Data for Name: t_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_cliente VALUES (true, '1234', 1);
INSERT INTO public.t_cliente VALUES (true, '5678', 2);
INSERT INTO public.t_cliente VALUES (true, '1245', 3);


--
-- TOC entry 3331 (class 0 OID 24663)
-- Dependencies: 211
-- Data for Name: t_cuenta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_cuenta VALUES (4, 1, true, '478758', 2000, 'Ahorros');
INSERT INTO public.t_cuenta VALUES (6, 3, true, '495878', 0, 'Ahorros');
INSERT INTO public.t_cuenta VALUES (7, 2, true, '496825', 540, 'Ahorros');
INSERT INTO public.t_cuenta VALUES (8, 1, true, '585545', 1000, 'Corriente');
INSERT INTO public.t_cuenta VALUES (5, 2, true, '225487', 100, 'Corriente');


--
-- TOC entry 3332 (class 0 OID 24670)
-- Dependencies: 212
-- Data for Name: t_movimientos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_movimientos VALUES (9, 4, true, '2022-07-10 00:00:00', 1425, 'Retiro', -575);
INSERT INTO public.t_movimientos VALUES (10, 5, true, '2022-02-10 00:00:00', 700, 'Deposito', 600);
INSERT INTO public.t_movimientos VALUES (11, 6, true, '2022-07-10 00:00:00', 150, 'Deposito', 150);
INSERT INTO public.t_movimientos VALUES (12, 7, true, '2022-02-08 00:00:00', 0, 'Retiro', -540);


--
-- TOC entry 3333 (class 0 OID 24675)
-- Dependencies: 213
-- Data for Name: t_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_persona VALUES (2, 'Amazonas y  NNUU', NULL, 'Femenino', '0706418515', 'Marianela Montalvo', '097548965');
INSERT INTO public.t_persona VALUES (3, '13 junio y Equinoccial', NULL, 'Masculino', '0706418516', 'Juan Osorio', '098874587');
INSERT INTO public.t_persona VALUES (1, 'Otavalo sn y principal', NULL, 'Masculino', '0706418514', 'Jose Lema', '098254785');


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 209
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 12, true);


--
-- TOC entry 3180 (class 2606 OID 24662)
-- Name: t_cliente t_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_cliente
    ADD CONSTRAINT t_cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 24669)
-- Name: t_cuenta t_cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_cuenta
    ADD CONSTRAINT t_cuenta_pkey PRIMARY KEY (id);


--
-- TOC entry 3184 (class 2606 OID 24674)
-- Name: t_movimientos t_movimientos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_movimientos
    ADD CONSTRAINT t_movimientos_pkey PRIMARY KEY (id);


--
-- TOC entry 3186 (class 2606 OID 24681)
-- Name: t_persona t_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_persona
    ADD CONSTRAINT t_persona_pkey PRIMARY KEY (id);


--
-- TOC entry 3189 (class 2606 OID 24692)
-- Name: t_movimientos fkeu1dugav9agn0jyo3ot3gwwpm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_movimientos
    ADD CONSTRAINT fkeu1dugav9agn0jyo3ot3gwwpm FOREIGN KEY (cuentaid) REFERENCES public.t_cuenta(id);


--
-- TOC entry 3188 (class 2606 OID 24687)
-- Name: t_cuenta fkffy1mcq63hanalp7f28v7s9dm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_cuenta
    ADD CONSTRAINT fkffy1mcq63hanalp7f28v7s9dm FOREIGN KEY (clienteid) REFERENCES public.t_cliente(id);


--
-- TOC entry 3187 (class 2606 OID 24682)
-- Name: t_cliente fkl737b10h5j91wjn9vthffthgh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_cliente
    ADD CONSTRAINT fkl737b10h5j91wjn9vthffthgh FOREIGN KEY (id) REFERENCES public.t_persona(id);


-- Completed on 2022-07-10 16:05:33 -05

--
-- PostgreSQL database dump complete
--

